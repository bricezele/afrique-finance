import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {fetchPostById} from "@redux/actions";
import {View} from "react-native";
import Spinkit from "@components/Spinkit";
import styles from "./styles";


class PostDetailLoading extends PureComponent {
    static propTypes = {
        id: PropTypes.number,
        goToScreen: PropTypes.func,
    };

    componentDidMount() {
        const {id, goToScreen} = this.props;

        this.props.fetchPostById(id).then((post) => {
            console.log(post);
            goToScreen('postDetail',
                {
                    post: post,
                    index: 0,
                    parentPosts: [post]
                });
        });

    }

    render() {
        return (
            <View style={styles.loadingContainer}>
                <Spinkit/>
            </View>
        )
    }
}


export default connect(
    null,
    {fetchPostById}
)(PostDetailLoading);
