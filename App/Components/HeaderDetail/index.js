import React from 'react'
import {Dimensions, Image, Platform, Share, TouchableOpacity, View} from 'react-native'
import styles from './style'
import {Images, Config} from '@common'
import {connect} from 'react-redux'
import {fetchPostsBookmark} from '@redux/actions'
import User from '@services/User'
import Indicator from './Indicator'
import Tools from "../../Common/Tools"
import _ from 'lodash';

const PAGE_WIDTH = Dimensions.get('window').width;
const INDICATOR_NUMBER = Math.round(PAGE_WIDTH / 24);

class HeaderDetail extends React.Component {

    state = {
        clicked: false
    };

    render() {
        console.log(PAGE_WIDTH);
        let {onBack, onMore, parentPosts, scrollX, post} = this.props;
        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity style={styles.button} hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}
                                      onPress={onBack}>

                        {Platform.OS === 'android' ?
                            <Image source={Images.WhiteBackIconAndroid} style={styles.backIcon}/>
                            :
                            <Image source={Images.WhiteBackIconIos} style={styles.backIcon}/>
                        }
                    </TouchableOpacity>
                </View>

                <View style={{marginLeft: -20}}>
                    {Platform.OS === 'ios' && Config.displayIndicator &&
                    <Indicator items={_.slice(parentPosts, 0, INDICATOR_NUMBER)} scrollX={scrollX}/>}

                </View>

                <View style={styles.postDetailOptionContainer}>
                    <TouchableOpacity style={{right: 50}} hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}
                                      onPress={this.shareText}>
                        <Image source={Images.ShareIcon} style={[styles.icon, {tintColor: 'white'}]}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{right: 20}} hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}
                                      onPress={this.readLater}>
                        <Image source={this.state.clicked ? Images.FavoritedIconWhite : Images.FavoriteIconWhite}
                               style={[styles.icon, !this.state.clicked && {tintColor: 'white'}]}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    componentWillMount() {
        this.checkClicked(this.props.posts);
        User.isSaved(this.props.post).then(status => {
            console.log("Statut article", status);
            this.setState({
                clicked: status
            })
        });
    }

    checkClicked(posts) {
        const postid = this.props.post && this.props.post.id;
        let clicked = (posts.filter(item => postid == item.id)) == "";
        this.setState({clicked: !clicked});
    }

    shareText = () => {
        const post = this.props.post ? this.props.post : '';
        const url = post.link;
        const title =
            typeof post.title.rendered !== 'undefined'
                ? Tools.formatText(post.title.rendered, 300)
                : '';

        Share.share(
            {
                message: title,
                url,
                title,
            },
            {
                dialogTitle: `${title} ${url}`,
                excludedActivityTypes: ['com.apple.UIKit.activity.PostToTwitter'],
                tintColor: 'blue',
            }
        ).catch((error) => this.setState({result: `error: ${error.message}`}))
    };

    readLater = () => {
        if (this.state.clicked) {
            this.setState({clicked: false});
            User.removePost(this.props.post, this.props.fetchPostsBookmark);
            return;
        }

        this.setState({clicked: true});
        User.savePost(this.props.post, this.props.fetchPostsBookmark);
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.posts != undefined && nextProps.posts != undefined && nextProps.posts.length != this.props.posts.length) {
            this.checkClicked(nextProps.posts);
        }
        User.isSaved(nextProps.post).then(status => {
            console.log("Statut article", status);
            this.setState({
                clicked: status
            })
        });
    }

}

HeaderDetail.defaultProps = {
    posts: []
};

const mapStateToProps = ({bookmark, user}, ownProps) => {
    const postId = ownProps.post.id;

    return {posts: bookmark.posts, data: user.data}
};

export default connect(mapStateToProps, {fetchPostsBookmark})(HeaderDetail)
