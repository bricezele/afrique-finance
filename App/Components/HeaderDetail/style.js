/** @format */

import {StyleSheet} from 'react-native'
import {Color} from "../../Common";

export default StyleSheet.create({
    container: {
        height: 50,
        backgroundColor: Color.main,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    postDetailOptionContainer: {
        width: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
        
    },
    backIcon: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    }
})
