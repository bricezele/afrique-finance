/** @format */

import {
    CREATE_POST_FAIL,
    CREATE_POST_PENDING,
    CREATE_POST_SUCCESS,
    FETCH_PENDING,
    MAGAZINE_FETCH_MORE,
    MAGAZINE_FETCH_SUCCESS,
    PHOTO_FETCH_MORE,
    PHOTO_FETCH_SUCCESS,
    POST_CHANGE_LAYOUT_SUCCESS,
    POST_FETCH_MORE,
    POST_FETCH_SUCCESS,
    POST_ID_FETCH_SUCCESS,
    POST_RELATED_FETCH_SUCCESS,
    SEARCH_POSTS_SUCCESS,
    STICKY_FETCH_SUCCESS,
    VIDEO_FETCH_MORE,
    VIDEO_FETCH_SUCCESS
} from '@redux/types'

import {flatten} from 'lodash'

const initialState = {
    isFetching: true,
    postFinish: false,
    error: null,
    list: [],
    sticky: [],

    // need refactor
    videos: [],
    photos: [],
    related: [],
    magazines: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PENDING:
            return {
                ...state,
                isFetching: true,
                error: null,
            }

        case POST_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                list: flatten(action.payload),
            }

        case POST_FETCH_MORE:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                list: state.list.concat(flatten(action.payload)),
            }

        case VIDEO_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                videos: flatten(action.payload),
            }
        case VIDEO_FETCH_MORE:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                videos: state.videos.concat(flatten(action.payload)),
            }

        case MAGAZINE_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                magazines: flatten(action.payload),
            }

        case MAGAZINE_FETCH_MORE:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                magazines: state.magazines.concat(flatten(action.payload)),
            }

        case PHOTO_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                photos: flatten(action.payload),
            }

        case PHOTO_FETCH_MORE:
            return {
                ...state,
                error: null,
                postFinish: action.finish,
                isFetching: false,
                photos: state.photos.concat(flatten(action.payload)),
            }
        case POST_CHANGE_LAYOUT_SUCCESS:
            return {
                ...state,
                error: null,
                isFetching: false,
                layout: action.layout,
            }

        case STICKY_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                isFetching: false,
                sticky: action.payload,
            }


        case POST_RELATED_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                isFetching: false,
                related: action.payload,
            }

        case SEARCH_POSTS_SUCCESS:
            return {
                ...state,
                error: null,
                isFetching: false,
                postsInSearch: action.payload,
            }
        case CREATE_POST_PENDING:
            return {
                ...state,
                type: action.type
            }

        case CREATE_POST_SUCCESS:
            return {
                ...state,
                type: action.type
            }

        case CREATE_POST_FAIL:
            return {
                ...state,
                type: action.type,
                message: action.message
            }

        case POST_ID_FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                isFetching: false,
                list: [...state.list, {...action.payload}],
            }
        default:
            return state
    }
}
