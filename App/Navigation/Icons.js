/** @format */

import React, {Fragment} from 'react';
import {Dimensions, Image, Platform, StyleSheet, Text, TouchableOpacity,} from 'react-native';
import {Constants, Events, Images, Languages} from '@common';

const PAGE_WIDTH = Dimensions.get('window').width;
const vw = PAGE_WIDTH / 100;

const styles = StyleSheet.create({
    toolbarIcon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        marginTop: 2,
        marginRight: 12,
        marginBottom: 12,
        marginLeft: 12,
        opacity: 0.8,
    },

    longBack: {
        width: 25,
    },
    toolbarIconUser: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: 1,
        right: 20,
        left: !Constants.RTL && Platform.OS !== 'android' ? vw * 5 : 20,
        top: Platform.OS !== 'android' ? 5 : 10,
        zIndex: 9999,
        flex: 1,
    },
    clearText: {
        color: '#333',
        textDecorationLine: 'underline',
        marginRight: 0,
    },
    backIcon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        marginLeft: 10,
        marginTop: 5
    },
});

const hitSlop = {top: 10, right: 10, bottom: 10, left: 10}

const Home = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.openLeftMenu}>
        <Image source={{uri: Images.icons.home}} style={styles.toolbarIcon}/>
    </TouchableOpacity>
);

const Layer = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.openModalLayout}>
        <Image
            source={{uri: Images.icons.layer}}
            style={[styles.toolbarIcon, {marginTop: 6, marginRight: 6}]}
        />
    </TouchableOpacity>
);

const Back = (func, iconBack) => (
    <TouchableOpacity hitSlop={hitSlop} onPress={func}>
        {iconBack ? (
            <Image
                source={iconBack}
                style={[{tintColor: '#fff'}, styles.toolbarIcon, styles.longBack]}
            />
        ) : (
            <Fragment>
                {Platform.OS === 'android' ?
                    <Image source={Images.WhiteBackIconAndroid} style={styles.backIcon}/>
                    :
                    <Image source={Images.WhiteBackIconIos} style={styles.backIcon}/>
                }
            </Fragment>
        )}
    </TouchableOpacity>
);

const Next = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.nextPost}>
        <Image
            source={{uri: Images.icons.next}}
            style={[
                styles.toolbarIcon,
                {width: 60, height: 12, marginRight: 0, marginTop: 18, opacity: 0.8},
            ]}
        />
    </TouchableOpacity>
);

const Clear = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.clearPosts}>
        <Text style={styles.clearText}>{Languages.clear}</Text>
    </TouchableOpacity>
);

const User = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.openUserModal}>
        <Image source={{uri: Images.icons.user}} style={[styles.toolbarIcon]}/>
    </TouchableOpacity>
);

const Logout = () => (
    <TouchableOpacity hitSlop={hitSlop} onPress={Events.logoutUser}>
        <Image source={{uri: Images.icons.logout}} style={styles.toolbarIcon}/>
    </TouchableOpacity>
);

export default {Home, Layer, Next, User, Clear, Back, Logout}
