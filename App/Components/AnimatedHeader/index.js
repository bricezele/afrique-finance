/** @format */

import React, {Component} from 'react';
import {Animated, Dimensions, View} from 'react-native';
import Icons from '@navigation/Icons';
import styles from './styles';
import {Constants, Images} from '@common';

const headerMinHeight = 50;
const {width} = Dimensions.get('window');

export default class AnimatedHeader extends Component {

    render() {
        const {scrollY, label, goBack, image, right} = this.props;

        const titleTransformY = scrollY.interpolate({
            inputRange: [0, headerMinHeight],
            outputRange: [0, -40],
            extrapolate: 'clamp',
        });
        const titleTransformX = scrollY.interpolate({
            inputRange: [0, headerMinHeight],
            outputRange: [0, Constants.RTL ? -25 : 25],
            extrapolate: 'clamp',
        });
        const titleScale = scrollY.interpolate({
            inputRange: [0, headerMinHeight],
            outputRange: [1, 0.8],
            extrapolate: 'clamp',
        });
        const titleOpacity = scrollY.interpolate({
            inputRange: [0, headerMinHeight * 1.5],
            outputRange: [1, 0]
        });

        return (
            <View style={styles.body}>
                <Animated.View
                    style={styles.headerView}
                />

                {label && (
                    <>
                        <Animated.Text
                            style={[
                                styles.headerLabel,
                                {
                                    zIndex: 9,
                                    color: '#fff',
                                    left: 0,
                                    position: 'absolute',
                                    transform: [
                                        {translateY: titleTransformY},
                                        {translateX: titleTransformX},
                                        {scale: titleScale},
                                    ],
                                }
                            ]}>
                            {label}
                        </Animated.Text>
                        <Animated.Text
                            style={[
                                styles.headerLabel,
                                {
                                    zIndex: 10,
                                    color: '#000',
                                    left: 0,
                                    position: 'absolute',
                                    opacity: titleOpacity,
                                    transform: [
                                        {translateY: titleTransformY},
                                        {translateX: titleTransformX},
                                        {scale: titleScale}
                                    ],
                                }
                            ]}>
                            {label}
                        </Animated.Text>
                    </>
                )}

                {image && (
                    <Animated.Image
                        source={image}
                        style={[
                            styles.headerImage,
                            {
                                transform: [
                                    {translateY: titleTransformY},
                                    {translateX: titleTransformX},
                                    {scale: titleScale},
                                    {opacity: titleOpacity}
                                ],
                            },
                        ]}
                    />
                )}

                {typeof goBack != 'undefined' ? (
                    <View style={[styles.homeMenu,]}>
                        {Icons.Back(goBack)}
                    </View>
                ) : (
                    <View style={[styles.homeMenu,]}>{Icons.Home()}</View>
                )}

                {right && (
                    <Animated.View
                        style={[
                            styles.headerRight,
                            {transform: [{translateY: titleTransformY}]}
                        ]}>
                        {right}
                    </Animated.View>
                )}
            </View>
        )
    }
}
