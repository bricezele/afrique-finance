/** @format */

import {Dimensions, StyleSheet} from 'react-native'

const {width, height} = Dimensions.get('window');
const vh = height / 100

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    text: {
        fontSize: 15,
        color: "#000000",
        textAlign: 'center',
        marginTop: 60
    },
});
