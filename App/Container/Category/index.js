/** @format */

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Animated, ScrollView, Text, View} from 'react-native'
import _, {flatten} from 'lodash'
import {fetchCategories, setActiveCategory, setActiveLayout,} from '@redux/actions'
import {connect} from 'react-redux'
import {Color, Config, Images, Languages} from '@common'
import {AnimatedHeader, CategoryList, TouchableScale} from '@components'
import FAB from '@custom/react-native-fab'
import Icon from '@expo/vector-icons/FontAwesome'
import styles from './styles'

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView)

class CategoryHome extends PureComponent {
    static propTypes = {
        categories: PropTypes.array,
        fetchCategories: PropTypes.func,
        setActiveCategory: PropTypes.func,
        onViewPost: PropTypes.func,
        onViewCategory: PropTypes.func,
        setActiveLayout: PropTypes.func,
        selectedLayout: PropTypes.bool,
    };

    state = {
        scrollY: new Animated.Value(0),
    };

    componentDidMount() {
        this.props.fetchCategories()
    }

    showCategory = (category) => {
        const {setActiveCategory, onViewCategory} = this.props
        setActiveCategory(category.id)

        onViewCategory({config: {name: category.name, category: category.id}})
    };

    changeLayout = () => {
        this.props.setActiveLayout(!this.props.selectedLayout)
    };

    indexToHeaxadecimal = () => {
        let letters = "0123456789ABCDEF";

        let color = '#';

        for (let i = 0; i < 6; i++)
            color += letters[(Math.floor(Math.random() * 16))];

        return color;
    };

    renderContent = () => {
        const {categories, onViewPost, selectedLayout} = this.props

        if (!selectedLayout) {
            return <CategoryList showBanner onViewPost={onViewPost}/>
        }

        return (
            <View>
                <AnimatedHeader scrollY={this.state.scrollY} label={Languages.category}/>
                <AnimatedScrollView
                    scrollEventThrottle={1}
                    contentContainerStyle={styles.scrollView}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true}
                    )}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'flex-start',
                        marginRight: 15
                    }}>
                        {typeof categories !== 'undefined' &&
                        categories.map((category, index) => {
                            let imageCategory = Config.imageCategories[category.slug]
                            if (imageCategory === undefined) {
                                imageCategory = Images.imageHolder
                            }
                            console.log(this.indexToHeaxadecimal());
                            return (
                                <View style={styles.containerStyle}>
                                    <TouchableScale style={styles.imageView} key={index + 'img'}
                                                    onPress={() => this.showCategory(category)}>
                                        {/*<Image
                                        style={[styles.image]}
                                        source={imageCategory}
                                    />*/}

                                        <View style={[styles.image, {
                                            backgroundColor: this.indexToHeaxadecimal()
                                        }]}/>

                                        <View style={styles.overlay}>
                                            <Text style={styles.title}>{_.upperFirst(category.name)}</Text>
                                            {category.description.length > 0 && (
                                                <Text numberOfLines={2} style={styles.description}>
                                                    {category.description}
                                                </Text>
                                            )}
                                        </View>

                                    </TouchableScale>
                                </View>
                            )
                        })}
                    </View>
                </AnimatedScrollView>
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {this.renderContent()}

                {Config.showSwitchCategory && (
                    <FAB
                        buttonColor={Color.toolbarTint}
                        iconTextColor="#FFFFFF"
                        onClickAction={this.changeLayout}
                        visible
                        iconTextComponent={<Icon name="exchange"/>}
                    />
                )}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const selectedCategory = state.categories.selectedCategory
    const categories = flatten(state.categories.list)
    const selectedLayout = state.categories.selectedLayout
    return {categories, selectedCategory, selectedLayout}
}
export default connect(mapStateToProps, {
    fetchCategories,
    setActiveLayout,
    setActiveCategory,
})(CategoryHome)
