/** @format */

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Config} from '@common'
import {fetchPostsByTag} from '@redux/actions'
import Constants from "../../Common/Constants";
import Horizontal from "./Horizontal";
import {Mansory, PostList} from "../../Components";

class Home extends PureComponent {
    static propTypes = {
        layout: PropTypes.number,
        onShowAll: PropTypes.func,
        onViewPost: PropTypes.func,
        fetchPostsByTag: PropTypes.func,
    };

    render() {
        const {layout, onShowAll, onViewPost, fetchPostsByTag} = this.props;
        let newLayout = layout;

        // this is for first layout config when load the app
        if (typeof newLayout === 'undefined') {
            newLayout = Config.homeLayout
        }

        switch (newLayout) {
            case Constants.Layout.mansory:
                return <Mansory onViewPost={onViewPost}/>
            case Constants.Layout.horizontal:
                return (
                    <Horizontal
                        onViewPost={onViewPost}
                        onShowAll={onShowAll}
                        fetchPostsByTag={fetchPostsByTag}
                    />
                );
            default:
                return <PostList onViewPost={onViewPost} showBanner layout={newLayout}/>
        }

    }
}

const mapStateToProps = ({posts}) => {
    return {
        layout: posts.layout,
    }
};

export default connect(mapStateToProps, {fetchPostsByTag})(Home)
