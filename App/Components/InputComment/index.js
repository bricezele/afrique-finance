/** @format */

import React, { PureComponent } from 'react'
import {View, Text, TouchableOpacity, Image, Modal, TextInput,KeyboardAvoidingView, TouchableWithoutFeedback, AsyncStorage, ActivityIndicator} from 'react-native'
import { Constants, Config, Images, Languages } from '@common'
import styles from './style'
import moment from 'moment'
import Comment from '@services/Comment'
import TextInputComment from './Input'

class InputComment extends PureComponent {
  state = {
    isVisible: false,
    loading: false
  }

  hide = ()=>{
    this.setState({isVisible: false})
  }

  show = ()=>{
    this.setState({isVisible: true})
  }

  render() {
    let {loading} = this.state
    return (
      <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isVisible}
          onRequestClose={this.hide}>
          <TouchableWithoutFeedback onPress={this.hide}>
            <View style={styles.backgroundColor} keyboardShouldPersistTaps={'handled'}>
              <TextInputComment post={this.props.post} onHide={this.finishPost}/>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
    )
  }

  finishPost = ()=>{
    this.hide()
    this.props.reloadComments()
  }
}

export default InputComment
