import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
