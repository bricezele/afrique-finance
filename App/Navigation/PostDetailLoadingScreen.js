import React, {Component} from 'react'
import {Dimensions, StyleSheet} from 'react-native'
import {PostDetailLoading} from '@container'

const PAGE_WIDTH = Dimensions.get('window').width

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: 40,
        paddingLeft: 8,
        paddingRight: 8,
        elevation: 0,
        paddingTop: 0,
        width: PAGE_WIDTH / 2,
    },
    container: {
        flex: 1,
    },
});


export default class PostDetailLoadingScreen extends Component {
    static navigationOptions = () => ({
        headerTintColor: '#333',
        // tabBarVisible: false,
        // headerLeft: Icons.Back( () => navigation.goBack() ),
        // headerRight: Icons.Next()
        headerStyle: styles.toolbar,
        header: null,
        tabBarVisible: false,
    });

    render() {
        const {state} = this.props.navigation;

        return (<PostDetailLoading
                id={state.params.id}
                goToScreen={state.params.goToScreen}/>
        );
    }

}
