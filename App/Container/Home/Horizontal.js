/** @format */

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Animated, RefreshControl, View} from 'react-native';
import {HorizonList, Spinkit} from '@components';
import {AppConfig} from '@common';
import styles from './styles';
import AnimatedHeader from "../../Components/AnimatedHeader";
import Constants from "../../Common/Constants";
import Config from "../../Common/Config";
import Icons from "../../Navigation/Icons";
import _ from "lodash";

class Horizontal extends PureComponent {

    constructor(props) {
        super(props);
        this.index = Config.categoriesNumberToPreload;
        this.loadMore = true;

        this.state = {
            scrollY: new Animated.Value(0),
            refreshing: false,
            layoutItems: _.slice(AppConfig.HorizonLayout, 0, this.index)
        };
    }

    static propTypes = {
        fetchPostsByTag: PropTypes.func,
        onShowAll: PropTypes.func,
        onViewPost: PropTypes.func,
        goBack: PropTypes.func,
    };


    onRefresh = () => {
        this.setState({refreshing: true});
        AppConfig.HorizonLayout.map((config, index) => {
            this.props.fetchPostsByTag(1, config.tags, config.categories, config.sticky, index);
        });
        setTimeout(() => {
            this.setState({refreshing: false})
        }, 600)
    };

    nextPosts = () => {
        this.index += 1;
        if (this.index === _.size(AppConfig.HorizonLayout)) this.loadMore = false;
        else {
            this.setState({
                layoutItems: _.slice(AppConfig.HorizonLayout, 0, this.index)
            });
        }
    };

    isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
    };

    renderFooter = () => {
        return <Spinkit/>;
    };

    render() {
        console.log("Layout Item", this.state.layoutItems);
        const {onShowAll, onViewPost, goBack} = this.props;

        return (
            <View style={styles.body}>

                {<AnimatedHeader
                    label={Constants.AppName}
                    right={Config.showLayoutButton && Icons.Layer()}
                    scrollY={this.state.scrollY}
                />}

                <Animated.ScrollView
                    contentContainerStyle={styles.scrollView}
                    scrollEventThrottle={1}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh}
                        />
                    }
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {
                            listener: (event) => {
                                if (this.isCloseToBottom(event.nativeEvent)) {
                                    this.nextPosts();
                                }
                            },
                            useNativeDriver: true
                        }
                    )}
                >
                    {this.state.layoutItems.map((config, index) => (
                        <HorizonList
                            horizontal
                            key={`hlist-${index}`}
                            config={config}
                            index={index}
                            onBack={() => goBack()}
                            onShowAll={onShowAll}
                            onViewPost={onViewPost}
                        />
                    ))}

                    {this.loadMore && this.renderFooter()}
                </Animated.ScrollView>
            </View>
        )
    }
}

export default Horizontal
