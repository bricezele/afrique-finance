import React, {Component} from "react";
import PropTypes from 'prop-types'
import {LogIn} from '@container';
import {Animated, FlatList, Text, View} from "react-native";
import {connect} from "react-redux";
import {fetchMagazines, fetchUserData} from '@redux/actions';
import {Languages} from "@common";
import styles from './styles';


const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class Magazine extends Component {

    static propTypes = {
        fetchMagazines: PropTypes.func,
        onViewPost: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.page = 1;
        this.state = {
            scrollY: new Animated.Value(0),
        }
    }

    fetchPostMagazines = () => {
        this.props.fetchMagazines(this.page);
    }

    nextMagazines = () => {
        this.page += 1;
        this.fetchPostMagazines();
    }

    componentWillMount() {
        this.fetchPostMagazines();
        if (typeof this.props.user.data == 'undefined') {
            this.props.fetchUserData();
        }
    }

    render() {

        const {onViewMagazine, user} = this.props;

        if (typeof user.data != 'undefined') {
            return <View/>;
        } else {
            return (
                <View style={styles.container}>
                    <Text style={styles.text}>{Languages.connectFirst}</Text>
                    <LogIn/>
                </View>
            );
        }
    }
}

const mapStateToProps = ({user, posts}) => {
    console.log(posts);
    return {
        user: user,
        magazines: posts.magazines
    }
};
export default connect(mapStateToProps, {fetchUserData, fetchMagazines})(Magazine);
