import React, {Component} from "react";
import {Dimensions} from "react-native";
import {Magazine} from '@container';
import PropTypes from "prop-types";
import Images from "@common/Images";
import Icons from '@navigation/Icons';
import {TabBarIcon} from "@components";

const {width} = Dimensions.get("window"), vw = width / 100;

export default class MagazineScreen extends Component {

    static navigationOptions = {
        title: 'Magazines',
        tabBarLabel: 'Magazines',
        tabBarIcon: ({tintColor}) => (
            <TabBarIcon icon={Images.icons.magazine} tintColor={tintColor}/>
        ),
        headerLeft: Icons.Home(),
    };

    static propTypes = {
        navigation: PropTypes.object.isRequired,
    }

    render = () => <Magazine/>
}

